<?php
namespace Shopwedo;

use Shopwedo\Exceptions\ShopwedoResponseException;
use Shopwedo\Exceptions\ShopwedoSDKException;
use Shopwedo\ShopwedoClient;

/**
 * class Shopwedo
 *
 * @package Shopwedo
 */
class Shopwedo
{
    protected $client;

    public function __construct(array $config = [])
    {
        $config += [
            'shop_id' => '',
            'api_key' => ''
        ];

        if (!$config['shop_id']) {
            throw new ShopwedoSDKException('Required "shop_id" key not set in config');
        }

        if (!$config['api_key']) {
            throw new ShopwedoSDKException('Required "api_key" key not set in config');
        }

        $this->client = new ShopwedoClient($config['shop_id'], $config['api_key']);
    }

    public function authTest()
    {
        $response = $this->client->request('authTest');

        return $response->getStatusCode() == 202;
    }

    public function __call($name, $arguments)
    {

        $data = [];
        if (isset($arguments[0])) {
            $data = $arguments[0];
        }

        $response = $this->client->request($name, $data);

        if (in_array($response->getStatusCode(), array(200, 201,202))) {
            if ($body = json_decode((string)$response->getBody(), true)) {
                return $body;
            }

            throw new ShopwedoSDKException(sprintf('"%s" response did not return a json string (%s)'), $name, (string)$response->getBody());
        }

        throw new ShopwedoResponseException(
            sprintf('"%s" returned with wrong statuscode (%s), expected %s', $name, $response->getStatusCode(), $expectedResponseCode)
        );
    }
}
